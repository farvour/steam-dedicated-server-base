FROM debian:bookworm
LABEL maintainer="Thomas Farvour <tom@farvour.com>"

ARG DEBIAN_FRONTEND=noninteractive

# Top level directory where everything related to the dedicated server is
# installed to. Since you can bind-mount data volumes for data, worlds, saves or
# other things, this doesn't really have to change, but is here for clarity and
# customization in case.

ENV SERVER_HOME=/opt/server
ENV SERVER_INSTALL_DIR=/opt/server/dedicated-server
ENV SERVER_DATA_DIR=/var/opt/server/data

# Use root user to install system packages and setup users.
USER root

# Steam still requires 32-bit cross compilation libraries.
RUN echo "Installing necessary system packages to support steam CLI installation" \
    && apt-get update \
    && apt-get -yy --no-install-recommends install \
    bash ca-certificates curl dumb-init expect git gosu htop lib32gcc-s1 net-tools \
    netcat-traditional pigz rsync telnet tmux unzip vim wget \
    && apt clean -y && rm -rf /var/lib/apt/lists/*

# Non-privileged user.
ENV PROC_UID 7777
ENV PROC_USER server
ENV PROC_GROUP nogroup

# Sets the proper locale variables for the environment.
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN echo "Create a non-privileged user to run with" \
    && useradd -u ${PROC_UID} -d ${SERVER_HOME} -g ${PROC_GROUP} ${PROC_USER}

RUN echo "Create server directories" \
    && mkdir -p ${SERVER_HOME} \
    && mkdir -p ${SERVER_INSTALL_DIR} \
    && mkdir -p ${SERVER_INSTALL_DIR}/Mods \
    && mkdir -p ${SERVER_DATA_DIR} \
    && mkdir -p ${SERVER_HOME}/Steam \
    && chown -R ${PROC_USER}:${PROC_GROUP} ${SERVER_HOME}

# Additional system packages for everything not for the Steam installation. This
# includes Python, pipenv, and the correct Pipfile lock deployed to the system
# installation of Python 3.

USER root

ENV ASDF_DIR /opt/asdf-vm
ENV ASDF_DATA_DIR ${ASDF_DIR}
ENV ASDF_BRANCH v0.14.0

RUN echo "Cloning asdf-vm ${ASDF_BRANCH} into ${ASDF_DIR}" \
    && git clone https://github.com/asdf-vm/asdf.git ${ASDF_DIR} --branch ${ASDF_BRANCH}

RUN cat >/etc/profile.d/asdf.sh <<EOF
export ASDF_DIR=$ASDF_DIR
export ASDF_DATA_DIR=$ASDF_DATA_DIR

. ${ASDF_DIR}/asdf.sh
EOF

# Installs Python 3.11 (useful for building tools or scripts for dedicated servers).
ENV PYTHON_VERSION 3.11.9

RUN echo "Installing packages to support Python installation, run-time and entrypoint of server" \
    && apt-get update \
    && apt-get -yy --no-install-recommends install \
    build-essential libbz2-dev libffi-dev liblzma-dev libreadline-dev libssl-dev \
    libsqlite3-dev wget zlib1g-dev \
    && apt clean -y && rm -rf /var/lib/apt/lists/*

RUN echo "Installing Python ${PYTHON_VERSION} via asdf-vm" \
    && . /etc/profile.d/asdf.sh \
    && asdf plugin add python \
    && asdf install python ${PYTHON_VERSION} \
    && asdf global python ${PYTHON_VERSION}

# Installs pipenv to make virtualenv management easier.
ENV PIPENV_VENV_IN_PROJECT=1

RUN echo "Ensure pipenv is installed" \
    && . /etc/profile.d/asdf.sh \
    && python -V \
    && pip install pipenv

# Install the backup2l script for automatic backup assistance. You can simply
# run this script in the container and backup the mounted volume to a
# destination available on the host.

# docker run --rm --entrypoint /bin/bash -v 7dtd-dedicated-server_7dtd-data:/app/7dtd/data/Saves -v /mnt/backup:/app/7dtd/backups 7dtd-dedicated-server_7dtd-server:latest /app/7dtd/backup2l/backup2l -c /app/7dtd/backup2l/backup2l.conf -b

RUN echo "Install the backup2l tool and config" \
    && git clone --single-branch --branch master https://github.com/gkiefer/backup2l.git \
    && if [ ! -d ${SERVER_HOME}/backups ]; then mkdir -p ${SERVER_HOME}/backups; fi

COPY --chown=${PROC_USER}:${PROC_GROUP} config/backup2l.conf ${SERVER_HOME}/backup2l/backup2l.conf

# Install custom entrypoint script.
COPY scripts/entrypoint.sh /entrypoint.sh

# Set entrypoint and default command.
ENTRYPOINT ["/usr/bin/dumb-init", "--rewrite", "15:2", "--", "/entrypoint.sh"]
CMD ["/bin/bash"]

# Install steamcmd as the server user.
USER ${PROC_USER}
WORKDIR ${SERVER_HOME}

RUN echo "Downloading and installing steamcmd" \
    && cd Steam \
    && wget https://media.steampowered.com/installer/steamcmd_linux.tar.gz \
    && tar -zxvf steamcmd_linux.tar.gz \
    && chown -R ${PROC_USER}:${PROC_GROUP} . \
    && cd -

# Switch back to root user to allow entrypoint to drop privileges.
USER root
